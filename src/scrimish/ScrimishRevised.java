//Kelvin Wong
//SBU ID: 111090608

import java.util.ArrayList;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

public class ScrimishRevised {

	public static ArrayList<Integer> cardPile = new ArrayList<>();
	public static ArrayList<Integer> computer = new ArrayList<>();

	public static void reset() {
		cardPile.clear();
		for (int i = 0; i < 25; i++) {
			cardPile.add(0);
		}
	}

	public static void userGenerate() {

		reset();
		GUI.t.appendText("\nEnter five cards for pile 1: ");
		GUI.s.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				String s = GUI.s.getText();
				GUI.t.appendText(s);

				for (int i = 0, j = 0; i <= 20; i = i + 5, j = j + 2) {
					if ((s.charAt(j) == 'A' || s.charAt(j) == 'a')) {

						cardPile.set(i, 7);
					} else if ((s.charAt(j) == 'S' || s.charAt(j) == 's')) {

						cardPile.set(i, 8);
					} else if ((s.charAt(0) == 'C' || s.charAt(0) == 'c')) {
						cardPile.set(i, 9);
					} else {
						cardPile.set(i, Integer.parseInt("" + s.charAt(j)));
					}
				}

				GUI.t.appendText("\nEnter five cards for pile 2: ");
				GUI.s.clear();

				GUI.s.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent e) {

						String s = GUI.s.getText();
						GUI.t.appendText(s);

						for (int i = 1, j = 0; i <= 21; i = i + 5, j = j + 2) {
							if ((s.charAt(j) == 'A' || s.charAt(j) == 'a')) {

								cardPile.set(i, 7);
							} else if ((s.charAt(j) == 'S' || s.charAt(j) == 's')) {

								cardPile.set(i, 8);
							} else if ((s.charAt(0) == 'C' || s.charAt(0) == 'c')) {
								cardPile.set(i, 9);
							} else {
								cardPile.set(i, Integer.parseInt("" + s.charAt(j)));
							}
						}

						GUI.t.appendText("\nEnter five cards for pile 3: ");
						GUI.s.clear();

						GUI.s.setOnAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent e) {

								String s = GUI.s.getText();
								GUI.t.appendText(s);

								for (int i = 2, j = 0; i <= 22; i = i + 5, j = j + 2) {
									if ((s.charAt(j) == 'A' || s.charAt(j) == 'a')) {

										cardPile.set(i, 7);
									} else if ((s.charAt(j) == 'S' || s.charAt(j) == 's')) {

										cardPile.set(i, 8);
									} else if ((s.charAt(0) == 'C' || s.charAt(0) == 'c')) {
										cardPile.set(i, 9);
									} else {
										cardPile.set(i, Integer.parseInt("" + s.charAt(j)));
									}
								}

								GUI.t.appendText("\nEnter five cards for pile 4: ");
								GUI.s.clear();

								GUI.s.setOnAction(new EventHandler<ActionEvent>() {
									@Override
									public void handle(ActionEvent e) {

										String s = GUI.s.getText();
										GUI.t.appendText(s);

										for (int i = 3, j = 0; i <= 23; i = i + 5, j = j + 2) {
											if ((s.charAt(j) == 'A' || s.charAt(j) == 'a')) {

												cardPile.set(i, 7);
											} else if ((s.charAt(j) == 'S' || s.charAt(j) == 's')) {

												cardPile.set(i, 8);
											} else if ((s.charAt(0) == 'C' || s.charAt(0) == 'c')) {
												cardPile.set(i, 9);
											} else {
												cardPile.set(i, Integer.parseInt("" + s.charAt(j)));
											}
										}

										GUI.t.appendText("\nEnter five cards for pile 5: ");
										GUI.s.clear();

										GUI.s.setOnAction(new EventHandler<ActionEvent>() {
											@Override
											public void handle(ActionEvent e) {

												String s = GUI.s.getText();
												GUI.t.appendText(s);

												for (int i = 4, j = 0; i <= 24; i = i + 5, j = j + 2) {
													if ((s.charAt(j) == 'A' || s.charAt(j) == 'a')) {

														cardPile.set(i, 7);
													} else if ((s.charAt(j) == 'S' || s.charAt(j) == 's')) {

														cardPile.set(i, 8);
													} else if ((s.charAt(j) == 'C' || s.charAt(j) == 'c')) {
														cardPile.set(i, 9);
													} else {
														cardPile.set(i, Integer.parseInt("" + s.charAt(j)));
													}
												}

												GUI.s.clear();

												GUI.showGUICards();
												GUI.showComputerCards();
												GUI.t.appendText("\n----------Your Turn----------\n");
												selectCardsHuman();

											}
										});

									}
								});

							}
						});

					}
				});
			}
		});
	}

	public static void randomGenerator(ArrayList<Integer> s) {
		int c1 = 0;
		int c2 = 0;
		int c3 = 0;
		int c4 = 0;
		int c5 = 0;
		int c6 = 0;
		int c7 = 0;
		int c8 = 0;
		int c9 = 0;
		for (int i = 0; i < 25; i++) {
			if (i < 20) {
				boolean check = true;
				while (check) {
					int randomNum = randomNumberGenerator(1, 8);
					if (randomNum == 1 && c1 != 5) {
						c1++;
						s.add(1);
						check = false;
					} else if (randomNum == 2 && c2 != 5) {
						c2++;
						s.add(2);
						check = false;
					} else if (randomNum == 3 && c3 != 3) {
						c3++;
						s.add(3);
						check = false;
					} else if (randomNum == 4 && c4 != 3) {
						c4++;
						s.add(4);
						check = false;
					} else if (randomNum == 5 && c5 != 2) {
						c5++;
						s.add(5);
						check = false;
					} else if (randomNum == 6 && c6 != 2) {
						c6++;
						s.add(6);
						check = false;
					} else if (randomNum == 7 && c7 != 2) {
						c7++;
						s.add(7);
						check = false;
					} else if (randomNum == 8 && c8 != 2) {
						c8++;
						s.add(8);
						check = false;
					}
				}
				check = true;

			} else {
				boolean check2 = true;
				while (check2) {
					int randomNum = randomNumberGenerator(1, 9);
					if (randomNum == 1 && c1 != 5) {
						c1++;
						s.add(1);
						check2 = false;
					} else if (randomNum == 2 && c2 != 5) {
						c2++;
						s.add(2);
						check2 = false;
					} else if (randomNum == 3 && c3 != 3) {
						c3++;
						s.add(3);
						check2 = false;
					} else if (randomNum == 4 && c4 != 3) {
						c4++;
						s.add(4);
						check2 = false;
					} else if (randomNum == 5 && c5 != 2) {
						c5++;
						s.add(5);
						check2 = false;
					} else if (randomNum == 6 && c6 != 2) {
						c6++;
						s.add(6);
						check2 = false;
					} else if (randomNum == 7 && c7 != 2) {
						c7++;
						s.add(7);
						check2 = false;
					} else if (randomNum == 8 && c8 != 2) {
						c8++;
						s.add(8);
						check2 = false;
					} else if (randomNum == 9 && c9 != 1) {
						c9++;
						s.add(9);
						check2 = false;
					}
				}
				check2 = true;
			}
		}
	}

	public static int randomNumberGenerator(int min, int max) {
		return min + (int) (Math.random() * max);
	}

	public static void viewCurrentCards(ArrayList<Integer> s) {
		for (int i = 0; i < s.size(); i++) {
			if ((i + 1) % 5 == 0) {

				if (s.get(i) == 7) {
					GUI.t.appendText("A \n");
				} else if (s.get(i) == 8) {
					GUI.t.appendText("S \n");
				} else if (s.get(i) == 9) {
					GUI.t.appendText("C \n");
				} else {
					GUI.t.appendText(s.get(i) + " \n");
				}

			} else {

				if (s.get(i) == 7) {
					GUI.t.appendText("A ");
				} else if (s.get(i) == 8) {
					GUI.t.appendText("S ");
				} else if (s.get(i) == 9) {
					GUI.t.appendText("C ");
				} else {
					GUI.t.appendText(s.get(i) + " ");
				}
			}
		}
	}

	public static void selectCardsHuman() {

		GUI.t.appendText("Enter 0 to remove a card or choose a pile from \nyour cards (1-5): ");

		GUI.s.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				String temp = GUI.s.getText();
				int userPile = Integer.parseInt(temp);
				GUI.t.appendText(temp);
				GUI.s.clear();
				if (userPile == 0) {
					GUI.t.appendText("\nChoose a pile to remove your card (1-5): ");
					GUI.s.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent e) {

							String r = GUI.s.getText();
							int remove = Integer.parseInt(r);
							GUI.t.appendText(r + "\n");
							GUI.s.clear();

							remove(remove, cardPile);

							GUI.showGUICards();
							GUI.showComputerCards();
						
							GUI.t.appendText("\n----------Computer's Turn----------\n");
							GUI.t.appendText("The computer is thinking...\n\n");
							Timeline t = new Timeline(new KeyFrame(
							        Duration.seconds(5),
							        ae -> selectCardsComputer()));
							t.play();
					
							
						}
					});

				} else {

					int currentCard = getCardPile(userPile, cardPile);
					int currentIndex = currentIndex(userPile, cardPile);

					GUI.t.appendText("\nEnter the pile from the computer's cards you would \nlike to attack (1-5): ");

					GUI.s.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent e) {
							String temp = GUI.s.getText();
							int computerPile = Integer.parseInt(temp);
							GUI.t.appendText(temp + "\n\n");
							GUI.s.clear();

							int computerCard = getCardPile(computerPile, computer);
							int currentComputerIndex = currentIndex(computerPile, computer);

							if (currentCard == 0 || computerCard == 0) {
								GUI.t.appendText("You selected a pile with no cards. Try again\n");
								selectCardsHuman();
							 } else if (currentCard ==8){
								GUI.t.appendText("You cannot use a shield card to attack. Try again\n\n");
								selectCardsHuman();
							 } else if (currentCard == 9 && computerCard == 8) {
								GUI.t.appendText("User: " + viewCards(currentCard) + "\n");
								GUI.t.appendText("Computer: " + viewCards(computerCard) + "\n");
								GUI.t.appendText("STATUS: The computer wins the game!\n");

							} else if (currentCard == 9 || computerCard == 9) {
								GUI.t.appendText("User: " + viewCards(currentCard) + "\n");
								GUI.t.appendText("Computer: " + viewCards(computerCard) + "\n");
								GUI.t.appendText("STATUS: You win this game!\n");

							} else {
								attackForHuman(currentCard, computerCard, currentIndex,
										currentComputerIndex, userPile, computerPile);

									GUI.showGUICards();
									GUI.showComputerCards();
									
									GUI.t.appendText("\n----------Computer's Turn----------\n");
									GUI.t.appendText("The computer is thinking...\n\n");
									Timeline t = new Timeline(new KeyFrame(
									        Duration.seconds(5),
									        ae -> selectCardsComputer()));
									t.play();

							}

						}
					});

				}

			}
		});

	}

	public static String viewCards(int n) {
		switch (n) {
		case 1:
			return "1 – Dagger Card";
		case 2:
			return "2 – Sword Card";
		case 3:
			return "3 – Morning Star Card";
		case 4:
			return "4 – War Axe Card";
		case 5:
			return "5 – Halberd Card";
		case 6:
			return "6 – Longsword Card";
		case 7:
			return "A – Archer Card";
		case 8:
			return "S – Shield Card";
		case 9:
			return "C – Crown Card";
		default:
			return "";
		}
	}

	public static void selectCardsComputer() {
		
		int randomC;
		int pileNumber;

		randomC = randomNumberGenerator(0, 6);
		pileNumber = randomNumberGenerator(1, 5);
		int currentCard = getCardPile(pileNumber, cardPile);
		int currentIndex = currentIndex(pileNumber, cardPile);
		int computerCard = getCardPile(randomC, computer);
		int currentComputerIndex = currentIndex(randomC, computer);
		
		if (randomC == 0 && computerCard == 0){
			selectCardsComputer();
		} else if (randomC == 0) {
			GUI.t.appendText("The computer removed its card from pile " + pileNumber + "\n");
			remove(pileNumber, computer);
			GUI.showGUICards();
			GUI.showComputerCards();
			GUI.t.appendText("\n----------Your Turn----------\n");
			selectCardsHuman();

		} else {
			if (currentCard == 0 || computerCard == 0) {
				selectCardsComputer();
			} else if(computerCard == 8){
				selectCardsComputer();
				
			}else if (currentCard == 8 && computerCard == 9) {

				GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + randomC + "\n");
				GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + pileNumber + "\n");
				GUI.t.appendText("STATUS: You win this game!\n");

			} else if (computerCard == 9 || currentCard == 9) {

				GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + randomC + "\n");
				GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + pileNumber + "\n");
				GUI.t.appendText("STATUS: Sorry! You lost the game.");

			} else {
				attackForComputer(currentCard, computerCard, currentIndex, currentComputerIndex, pileNumber,
						randomC);
					GUI.showGUICards();
					GUI.showComputerCards();
					GUI.t.appendText("\n----------Your Turn----------\n");
					selectCardsHuman();
			}
		}
	}

	public static boolean attackForComputer(int currentCard, int computerCard, int currentIndex,
			int currentComputerIndex, int humanPileNumber, int computerPileNumber) {
		if (computerCard == 7 && currentCard == 8) {
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("STATUS: No cards have been discarded.\n");

			return true;
		} else if (computerCard == 7) {
			cardPile.set(currentIndex, 0);
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("STATUS: Your card has been discarded.\n");
			return true;
		} else if (currentCard == 7) {
			cardPile.set(currentIndex, 0);
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("STATUS: Your card has been discarded.\n");
			return true;
		} else if (computerCard == 8) {
			computer.set(currentComputerIndex, 0);
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("STATUS: The computer's card has been discarded.\n");
			return false;
		} else if (currentCard == 8) {
			computer.set(currentComputerIndex, 0);
			cardPile.set(currentIndex, 0);
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("STATUS: Both cards have been removed.\n");
			return true;
		} else if (computerCard > currentCard) {
			cardPile.set(currentIndex, 0);
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("STATUS: Your card has been removed.\n");
			return true;
		} else if (currentCard > computerCard) {
			computer.set(currentComputerIndex, 0);
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("STATUS: The computer's card has been removed.\n");
			return false;
		} else if (currentCard == computerCard) {
			computer.set(currentComputerIndex, 0);
			cardPile.set(currentIndex, 0);

			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("STATUS: Both cards have been discarded.\n");
			return true;
		}
		return false;
	}

	public static boolean attackForHuman(int currentCard, int computerCard, int currentIndex, int currentComputerIndex,
			int humanPileNumber, int computerPileNumber) {
		if (currentCard == 7 && computerCard == 8) {
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("STATUS: No cards have been discarded.\n");
			return true;
		} else if (currentCard == 7) {
			computer.set(currentComputerIndex, 0);
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("STATUS: The computer's card has been discarded.\n");
			return true;
		} else if (computerCard == 7) {
			computer.set(currentComputerIndex, 0);
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("STATUS: The computer's card has been discarded.\n");

			return true;
		} else if (currentCard == 8) {
			/*cardPile.set(currentIndex, 0);
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");*/
			
			GUI.t.appendText("STATUS: You cannot use a shield card to attack.\n");
			return true;
		} else if (computerCard == 8) {
			cardPile.set(currentIndex, 0);
			computer.set(currentComputerIndex, 0);
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("STATUS: Both cards have been discarded.\n");

			return true;
		} else if (currentCard > computerCard) {
			computer.set(currentComputerIndex, 0);

			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("STATUS: The computer's card has been discarded.\n");

			return true;

		} else if (computerCard > currentCard) {
			cardPile.set(currentIndex, 0);
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("STATUS: Your card has been discarded.\n");
			return false;
		} else if (currentCard == computerCard) {
			computer.set(currentComputerIndex, 0);
			cardPile.set(currentIndex, 0);
			GUI.t.appendText("User: " + viewCards(currentCard) + " at pile " + humanPileNumber + "\n");
			GUI.t.appendText("Computer: " + viewCards(computerCard) + " at pile " + computerPileNumber + "\n");
			GUI.t.appendText("STATUS: Both cards have been discarded.\n");
			return true;
		}
		return false;

	}

	public static void remove(int pileNumber, ArrayList<Integer> s) {
		if (pileNumber == 1) {
			for (int i = 0; i <= 20; i = i + 5) {
				if (s.get(i) != 0) {
					s.set(i, 0);
					break;
				}
			}
		} else if (pileNumber == 2) {
			for (int i = 1; i <= 21; i = i + 5) {
				if (s.get(i) != 0) {
					s.set(i, 0);
					break;
				}
			}
		} else if (pileNumber == 3) {
			for (int i = 2; i <= 22; i = i + 5) {
				if (s.get(i) != 0) {
					s.set(i, 0);
					break;
				}
			}
		} else if (pileNumber == 4) {
			for (int i = 3; i <= 23; i = i + 5) {
				if (s.get(i) != 0) {
					s.set(i, 0);
					break;
				}
			}
		} else if (pileNumber == 5) {
			for (int i = 4; i <= 24; i = i + 5) {
				if (s.get(i) != 0) {
					s.set(i, 0);
					break;
				}
			}
		}

	}

	public static int currentIndex(int pileNumber, ArrayList<Integer> s) {

		if (pileNumber == 1) {
			for (int i = 0; i <= 20; i = i + 5) {
				if (s.get(i) != 0) {
					return i;
				}
			}
		} else if (pileNumber == 2) {
			for (int i = 1; i <= 21; i = i + 5) {
				if (s.get(i) != 0) {

					return i;
				}
			}
		} else if (pileNumber == 3) {
			for (int i = 2; i <= 22; i = i + 5) {
				if (s.get(i) != 0) {
					return i;
				}
			}
		} else if (pileNumber == 4) {
			for (int i = 3; i <= 23; i = i + 5) {
				if (s.get(i) != 0) {
					return i;
				}
			}
		} else if (pileNumber == 5) {
			for (int i = 4; i <= 24; i = i + 5) {
				if (s.get(i) != 0) {
					return i;
				}
			}
		}

		return 0;

	}

	public static int getCardPile(int pileNumber, ArrayList<Integer> s) {
		if (pileNumber == 1) {
			for (int i = 0; i <= 20; i = i + 5) {
				if (s.get(i) != 0) {
					return s.get(i);
				}
			}
		} else if (pileNumber == 2) {
			for (int i = 1; i <= 21; i = i + 5) {
				if (s.get(i) != 0) {

					return s.get(i);
				}
			}
		} else if (pileNumber == 3) {
			for (int i = 2; i <= 22; i = i + 5) {
				if (s.get(i) != 0) {
					return s.get(i);
				}
			}
		} else if (pileNumber == 4) {
			for (int i = 3; i <= 23; i = i + 5) {
				if (s.get(i) != 0) {
					return s.get(i);
				}
			}
		} else if (pileNumber == 5) {
			for (int i = 4; i <= 24; i = i + 5) {
				if (s.get(i) != 0) {
					return s.get(i);
				}
			}
		}

		return 0;

	}

	public static void censorComputerCards() {
		for (int i = 0; i < computer.size(); i++) {
			if (i == 24) {
				if (computer.get(i) == 0) {
					GUI.t.appendText("0 ");
				} else {
					GUI.t.appendText("– ");
				}
			} else if ((i + 1) % 5 == 0) {

				if (computer.get(i) == 0) {
					GUI.t.appendText("0 \n");
				} else {
					GUI.t.appendText("– \n");
				}

			} else {
				if (computer.get(i) == 0) {
					GUI.t.appendText("0 ");
				} else {
					GUI.t.appendText("– ");
				}

			}

		}
	}

	public static void play() {
		GUI.t.appendText(
				"\n\nPlease select from the following option:\n1) Manually enter cards\n2) Randomly generate cards ");
		cardPile.clear();
		computer.clear();

		GUI.s.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				String temp = GUI.s.getText();
				int s = Integer.parseInt(temp);
				GUI.t.appendText("\t" + temp + "\n");
				GUI.s.clear();
				if (s == 1) {
					GUI.t.appendText("\nUse the following key to enter your cards:\n" + "1 – Dagger Card (x5)\n"
							+ "2 – Sword Card (x5)\n" + "3 – Morning Card (x3)\n" + "4 – War Axe Card (x3)\n"
							+ "5 – Halberd Cards (x2)\n" + "6 – Longsword Cards (x2)\n" + "A – Archer Card (x2)\n"
							+ "S – Shield Card (x2)\n" + "C – Crown Card\n");
					randomGenerator(computer);
					userGenerate();
				} else if (s == 2) {
					randomGenerator(cardPile);
					randomGenerator(computer);
					GUI.showGUICards();
					GUI.showComputerCards();
					GUI.t.appendText("\n----------Your Turn----------\n");
					selectCardsHuman();
				} else if (s == 3) {
					System.exit(0);
				} else {
					GUI.t.appendText("Invalid input.\n");
				}
			}
		});

	}

}
