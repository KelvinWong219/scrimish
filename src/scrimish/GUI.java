//Kelvin Wong
//SBU ID: 111090608

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GUI extends Application {
	public static TextArea t = new TextArea();
	public static Button enter = new Button("Play Game");
	public static TextField s = new TextField();
	public static GridPane gridPane = new GridPane();
	public static GridPane gridPane2 = new GridPane();
	
	public void start(Stage primaryStage){
		BorderPane pane = new BorderPane();
		pane.setBottom(getHBox());
		pane.setCenter(getVBox());
		pane.setRight(getGridPane2());
		
		pane.setLeft(getGridPane());
		
		Scene scene = new Scene(pane);
		primaryStage.setTitle("Scrimish");
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	
	public static HBox getHBox(){
		HBox pane = new HBox();
		pane.setSpacing(20);
		pane.setAlignment(Pos.CENTER);
		pane.setPadding(new Insets(5,5,5,5));
		
		enter.setOnAction(e ->  ScrimishRevised.play());
		Button exit = new Button("Exit");
		exit.setOnAction(e -> System.exit(0));
		
		
		pane.getChildren().add(enter);
		pane.getChildren().add(new Label("User Input: "));
		pane.getChildren().add(s);
		
		pane.getChildren().add(exit);
		return pane;
	}
	
	public static VBox getVBox(){
		VBox v = new VBox();
		GUI.t.appendText("---------------Welcome to Scrimish!---------------\n");
		GUI.t.appendText("Click on the \"Play Game\" button to begin!");
		t.setPrefHeight(755);
		t.setStyle("-fx-font-size: 16px;");
		v.getChildren().add(t);
		return v;
	}
	
	public static GridPane getGridPane(){
		gridPane.setPadding(new Insets(0));
		gridPane.setHgap(1);
		gridPane.setVgap(1);
		gridPane.setStyle(" -fx-grid-lines-visible: true");
		int col = 0;
		int row = 0;
		
		
		for (int i = 0; i < 25; i++){
			int temp = 0;
			Image image = new Image("image/"+temp+".jpg");
			ImageView imageView = new ImageView();
			imageView.setImage(image);
			gridPane.add(imageView,  col++,row );
			
			if (col == 5){
				col=0;
				row++;
			}
		}
		return gridPane;

	}
	
	public static GridPane getGridPane2(){
		gridPane2.setPadding(new Insets(0));
		gridPane2.setHgap(1);
		gridPane2.setVgap(1);
		gridPane2.setStyle(" -fx-grid-lines-visible: true");
		int col = 0;
		int row = 0;
		
		
		for (int i = 0; i < 25; i++){
			int temp = 0;
			Image image = new Image("image/"+temp+".jpg");
			ImageView imageView = new ImageView();
			imageView.setImage(image);
			gridPane2.add(imageView,  col++,row );
			
			if (col == 5){
				col=0;
				row++;
			}
		}
		return gridPane2;

	}
	
	public static void showGUICards(){
		gridPane.getChildren().clear();
		int col = 0;
		int row = 0;
		for (int i = 0; i < ScrimishRevised.cardPile.size(); i++){
			int temp = ScrimishRevised.cardPile.get(i);
			Image image = new Image("image/"+temp+".jpg");
			ImageView imageView = new ImageView();
			imageView.setImage(image);
			gridPane.add(imageView,  col++,row );
			
			if (col == 5){
				col=0;
				row++;
			}
		}
	}
	
	public static void showComputerCards(){
		gridPane2.getChildren().clear();
		int col = 0;
		int row = 0;
		for (int i = 0; i < ScrimishRevised.computer.size(); i++){
			int temp = ScrimishRevised.computer.get(i);
			Image image;
			if (temp != 0){
				 image = new Image("image/backRed.jpg");
			} else {
				image = new Image("image/0.jpg");
			}
			ImageView imageView = new ImageView();
			imageView.setImage(image);
			gridPane2.add(imageView,  col++,row );
			
			if (col == 5){
				col=0;
				row++;
			}
		}
	}
	public static void main(String[] args){
		
		launch(args);
	}

}
